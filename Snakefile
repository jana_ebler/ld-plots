configfile: "config.json"
genotypes = config['vcf']
regions = config['regions']

window_size = 1000000
window_size_kb = 1000
 
results = 'ld-results'

rule all:
	input:
		expand('{results}/{region}/variants-{region}.vcf.gz', results=results, region=regions),
		expand('{results}/{region}/plot-{region}.pdf', results=results, region=regions)

# extract variant IDs in given region
rule extract_regions:
	input:
		genotypes
	output:
		'{results}/{region}/variants-{region}.vcf.gz'
	conda:
		'env/genotyping.yml'
	resources:
		mem_total_mb = 10000,
		runtime_hrs = 4
	shell:
		"bcftools view --min-ac 1 -r {wildcards.region} {input} | python3 scripts/annotate.py | bgzip > {output}"		

rule tabix:
	input:
		'{results}/{region}/variants-{region}.vcf.gz'
	output:
		'{results}/{region}/variants-{region}.vcf.gz.tbi'
	conda:
		"env/genotyping.yml"
	shell:
		"tabix -p vcf {input}"


rule plink_pre:
	input:
		vcf='{results}/{region}/variants-{region}.vcf.gz',
		tbi='{results}/{region}/variants-{region}.vcf.gz.tbi'
	output:
		"{results}/{region}/ld-{region}.bed"
	conda:
		"env/gatk4.yml"
	params:
		"{results}/{region}/ld-{region}"
	resources:
		mem_total_mb = 10000,
		runtime_hrs = 1,
	shell:
		"plink --vcf {input.vcf} --vcf-half-call m --out {params}"


rule plink_ld_region:
	input:
		plink="{results}/{region}/ld-{region}.bed"
	output:
		ld='{results}/{region}/ld-{region}.ld'
	conda:
		"env/gatk4.yml"
	params:
		"{results}/{region}/ld-{region}"
	threads:
		10
	resources:
		mem_total_mb = 60000,
		runtime_hrs = 2,
	shell:
		'plink --memory 50000 --threads {threads} --bfile {params} --r2 --ld-window-kb {window_size_kb} --ld-window-r2 0.0 --ld-window 100000 --with-freqs --out {wildcards.results}/{wildcards.region}/ld-{wildcards.region}'


rule get_positions:
	input:
		'{results}/{region}/variants-{region}.vcf.gz'
	output:
		'{results}/{region}/positions-{region}.tsv'
	shell:
		"zcat {input} | grep -v '#' | cut -f2,3 > {output}"

rule create_matrix:
	input:
		'{result}/{region}/ld-{region}.ld'
	output:
		'{result}/{region}/matrix-{region}.tsv'
	shell:
		"cat {input} | python3 scripts/create-ld-matrix.py > {output}"

rule create_ld_plot:
	input:
		matrix = '{results}/{region}/matrix-{region}.tsv',
		positions = '{results}/{region}/positions-{region}.tsv'
	output:
		'{results}/{region}/plot-{region}.pdf'
	conda:
		"env/genotyping.yml"
	shell:
		"python3 scripts/plot-ld-heatmap.py {input.matrix} {input.positions} {output}"

