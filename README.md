# LD plots #

Computes LD for given variants and plots the results.

### How to run ###

* set path to a bi-allelic VCF file with variants and genotypes in config.json
* specify the regions to consider in config.json
* run:  `` snakemake -j 10 --use-conda  `` 
* results will be written to  `` ld-results/  `` 