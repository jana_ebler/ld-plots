import sys

chromosomes = ['chr' + str(i) for i in range(1,23)] + ['chrX']
nr_lines = 0

for line in sys.stdin:
	if line.startswith('#'):
		print(line[:-1])
		continue
	fields = line.strip().split()
	if not fields[0] in chromosomes:
		continue
	assert len(fields[4].split(',')) == 1
	info = { i.split('=')[0] : i.split('=')[1] for i in fields[7].split(';')  }
	if 'ID' in info:
		fields[2] = info['ID']
	elif fields[2] == '.':
		# generate ID
		fields[2] = 'variant-' + str(nr_lines) 
	nr_lines += 1
	print('\t'.join(fields))
