import sys
from collections import defaultdict

def make_key(id1, id2):
	if id1 < id2:
		return (id1, id2)
	else:
		return (id2, id1)

def is_sv(var_id):
	if 'INS' in var_id:
		return True
	if 'DEL' in var_id:
		return True
	return False

id_to_pos = {}

# map [id1, id2] -> r^2
matrix = {}

for line in sys.stdin:
	if 'BP' in line:
		continue
	fields = line.split()
	var1 = fields[2]
	var2 = fields[6]
	pos1 = int(fields[1])
	pos2 = int(fields[5])

	assert fields[0] == fields[4]

	key = make_key(var1,var2)
	matrix[key] = fields[8]
	id_to_pos[var1] = [fields[0], pos1]
	id_to_pos[var2] = [fields[4], pos2]
	
# print
ids_full = sorted([ [i, f[0], f[1]] for i,f in id_to_pos.items() ], key=lambda x: x[2])
ids = [i[0] for i in ids_full]

print('\t'.join(['      '] + ids))

for id1 in ids:
	line = [id1]
	for id2 in ids:
		k = make_key(id1,id2)
		if k in matrix:
			line.append(matrix[k])
		else:
			line.append("nan")
	print('\t'.join(line))
